﻿using System;
using System.Collections.Generic;
using Plugin.Geolocator;
using Realms;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace GeoMapasUlatina.View
{
    public partial class GeoMapsPage : ContentPage
    {

        //Atributo
        public Realm _realm;

        public GeoMapsPage()
        {
            InitializeComponent();
            Title = "Mpas y Geolocalización";
            _realm = Helpers.UtilDB.GetInstanceRealm();

        }



        private async void Cargar_PIN(object sender, EventArgs e)
        {





            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;

            var position = await locator.GetPositionAsync();

            var lat = position.Latitude;

            var lon = position.Longitude;

            var pin = new Pin
            {
                
                Position = new Position(lat, lon),
                Label = "Mall San Pedro",
                Address = "San Pedro, Costa Rica"
            };

            MyMap.Pins.Add(pin);


        }

        protected async override void OnAppearing()
        {
           
            base.OnAppearing();

            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;

            var position = await locator.GetPositionAsync();

            var lat = position.Latitude;

            var lon = position.Longitude;

            var latN = 10.077327;

            var lonN = -84.315043;

            MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latN,lonN), Distance.FromKilometers(.5)));

            var pin = new Pin
            {

                Position = new Position(latN, lonN),
                Label = "Mall San Pedro",
                Address = "San Pedro, Costa Rica"
            };

            MyMap.Pins.Add(pin);
        }

    }
}
